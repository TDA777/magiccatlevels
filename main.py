import re
while True:
	fileread_non = open("123.json", "r+")
	fileread = fileread_non.read()
	firstlevel = int(input('Please, enter the level number: ')) - 1
	lastlevel = int(input('Please, enter the last level number: ')) - 1
	print('Hexes colours:\n\nEmpty = -1,\nViolet = 0,\nBlue = 1,\nLightBlue = 2,\nGreen = 3,\nYellow = 4,\nOrange = 5,\nRed = 6,\n\nAnimalCat = 8,\nAnimalBird = 9,\n\nSnowflakeCrystal = 10,\nSnowCrystal = 11,\nRainbowCrystal = 12,\nEmptyIce = 13,\nElectricCrystal = 14,\nDaughterBlotCrystal = 15,\nChargedCrystal = 16,\nChameleonCrystal = 17,\nBlotCrystal = 18\n')
	hextype = input('Colour: ')
	firstlevel_start = fileread.find('"Id": ' + str(firstlevel))
	lastlevel_start = fileread.find('"Id": ' + str(lastlevel)) - 2
	sample = fileread[firstlevel_start:lastlevel_start]
	regex = re.compile('\"H\":\s(\d+)')
	match = regex.findall(sample)
	count = 0
	for i in range(0,len(match)):
		if match[i] == str(hextype):
			count+=1
		else: pass
	print("Hexes count: " + str(count))
	print("Total hexes count: " + str(len(match)))